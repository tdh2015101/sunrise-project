// priority: 0

settings.logAddedRecipes = true
settings.logRemovedRecipes = true
settings.logSkippedRecipes = false
settings.logErroringRecipes = true

console.info('OK')

onEvent('recipes', event => {
	event.remove({output: 'avaritia:extreme_crafting_table'})
	event.remove({output: 'avaritia:neutron_collector'})
	event.remove({output: 'cyclic:hopper_fluid',type: 'minecraft:crafting'})
	event.remove({output: 'cyclic:screen',type: 'minecraft:crafting'})
	// Remove repeated recipes
	event.remove({output: 'minecraft:amethyst_shard', type: 'farmersdelight:cutting'})
	event.remove({output: 'minecraft:amethyst_shard', type: 'thermal:press'})
	event.remove({output: 'minecraft:soul_soil', type: 'cyclic:crusher'})
	event.remove({output: 'minecraft:soul_soil', type: 'blue_skies:alchemy'})
	event.remove({output: 'minecraft:warped_nylium', type: 'blue_skies:alchemy'})
	event.remove({output: 'minecraft:crimson_nylium', type: 'blue_skies:alchemy'})
	event.remove({input: 'minecraft:warped_nylium', type: 'blue_skies:alchemy'})
	event.remove({input: 'minecraft:crimson_nylium', type: 'blue_skies:alchemy'})
	event.remove({input: '#forge:stone', type: 'cyclic:packager'})
	event.remove({output: '#forge:stone', type: 'cyclic:packager'})
	event.remove({output: '#forge:gravel', type: 'cyclic:crusher'})
	event.remove({output: '#forge:gravel', type: 'minecraft:smelting'})
	event.remove({output: 'minecraft:redstone', type: 'minecraft:smelting'})
	event.remove({output: 'minecraft:redstone', type: 'minecraft:blasting'})
	event.remove({output: 'minecraft:amethyst_block', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:white_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:orange_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:magenta_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:light_blue_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:yellow_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:lime_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:pink_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:gray_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:light_gray_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:cyan_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:purple_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:blue_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:brown_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:green_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:red_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:black_stained_glass_pane', type: 'minecraft:crafting'})
	event.remove({output: '#quark:stools'})
	event.remove({output: '#create:seats'})
	event.remove({output: 'mekanism:dictionary'})
	event.remove({output: 'industrialforegoing:simple_black_hole_unit'})
	event.remove({output: 'croptopia:oatmeal'})
	// Fix the problem
	event.remove({output: 'minecraft:amethyst_shard', type: 'minecraft:crafting'})
	event.remove({output: 'minecraft:amethyst_shard', type: 'cyclic:crusher'})

	// Added Create Recipes
	event.recipes.createCrushing(['2x quark:red_corundum_pane', Item.of('quark:red_corundum_pane').withChance(0.1)], '3x quark:red_corundum_cluster')
	event.recipes.createCrushing(['2x quark:orange_corundum_pane', Item.of('quark:orange_corundum_pane').withChance(0.1)], '3x quark:orange_corundum_cluster')
	event.recipes.createCrushing(['2x quark:yellow_corundum_cluster', Item.of('quark:yellow_corundum_pane').withChance(0.1)], '3x quark:yellow_corundum_cluster')
	event.recipes.createCrushing(['2x quark:green_corundum_pane', Item.of('quark:green_corundum_cluster').withChance(0.1)], '3x quark:green_corundum_cluster')
	event.recipes.createCrushing(['2x quark:blue_corundum_pane', Item.of('quark:blue_corundum_pane').withChance(0.1)], '3x quark:blue_corundum_cluster')
	event.recipes.createCrushing(['2x quark:indigo_corundum_pane', Item.of('quark:indigo_corundum_pane').withChance(0.1)], '3x quark:indigo_corundum_cluster')
	event.recipes.createCrushing(['2x quark:violet_corundum_pane', Item.of('quark:violet_corundum_pane').withChance(0.1)], '3x quark:violet_corundum_cluster')
	event.recipes.createCrushing(['2x quark:white_corundum_pane', Item.of('quark:white_corundum_pane').withChance(0.1)], '3x quark:white_corundum_cluster')
	event.recipes.createCrushing(['15x create:crushed_osmium_ore', Item.of('10x create:experience_nugget').withChance(0.1)], '3x mekanism:block_raw_osmium')

	event.recipes.createCutting('16x pipez:energy_pipe', 'mekanism:basic_universal_cable').processingTime(800)
	event.recipes.createCutting('32x pipez:energy_pipe', 'mekanism:advanced_universal_cable').processingTime(800)

	event.recipes.createSequencedAssembly([
		Item.of('thermal:bitumen').withChance(11),
		Item.of('thermal:basalz_powder').withChance(50),
		Item.of('thermal:cinnabar').withChance(9),
		Item.of('thermal:sulfur').withChance(30)
	], 'create:scorchia', [
		event.recipes.createCutting('create:incomplete_precision_mechanism', 'create:incomplete_precision_mechanism').processingTime(100),
		event.recipes.createPressing('create:incomplete_precision_mechanism','create:incomplete_precision_mechanism'),
		event.recipes.createFilling('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', Fluid.of('minecraft:water',500)]),
		event.recipes.createPressing('create:incomplete_precision_mechanism','create:incomplete_precision_mechanism'),
		event.recipes.createDeploying('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism','create:andesite_alloy']),
		event.recipes.createPressing('create:incomplete_precision_mechanism','create:incomplete_precision_mechanism')
	]).transitionalItem('create:incomplete_precision_mechanism').loops(50)

	event.recipes.createSequencedAssembly([
		Item.of('create:shadow_steel').withChance(80),
		Item.of('minecraft:heavy_weighted_pressure_plate').withChance(10),
		Item.of('mekanism:dust_refined_obsidian').withChance(10)
	], 'create:precision_mechanism', [
		event.recipes.createFilling('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', Fluid.of('cofh_core:honey',500)]),
		event.recipes.createPressing('create:incomplete_precision_mechanism','create:incomplete_precision_mechanism'),

		event.recipes.createFilling('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', Fluid.of('mekanism:lithium',800)]),

		event.recipes.createFilling('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', Fluid.of('thermal:redstone',50)]),

		event.recipes.createDeploying('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism','create:andesite_alloy']),

		event.recipes.createFilling('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', Fluid.of('create:chocolate',500)]),
		event.recipes.createPressing('create:incomplete_precision_mechanism','create:incomplete_precision_mechanism'),

		event.recipes.createDeploying('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism','mekanism:dust_refined_obsidian']),

		event.recipes.createCutting('create:incomplete_precision_mechanism', 'create:incomplete_precision_mechanism').processingTime(900)
	]).transitionalItem('create:incomplete_precision_mechanism').loops(30)

		event.recipes.createSequencedAssembly([
		Item.of('create:red_seat').withChance(80),
		Item.of('quark:red_stool').withChance(20)
	], '#minecraft:slabs', [
		event.recipes.createCutting('create:incomplete_precision_mechanism', 'create:incomplete_precision_mechanism').processingTime(600),

		event.recipes.createFilling('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', Fluid.of('cofh_core:honey',50)]),

		event.recipes.createDeploying('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism','#minecraft:wool']),
		event.recipes.createPressing('create:incomplete_precision_mechanism','create:incomplete_precision_mechanism')
	]).transitionalItem('create:incomplete_precision_mechanism').loops(3)

	event.recipes.createSequencedAssembly([
		Item.of('mekanism:dictionary').withChance(60),
		Item.of('mekanism:alloy_infused').withChance(40)
	], 'mekanism:basic_control_circuit', [
		event.recipes.createCutting('create:incomplete_precision_mechanism', 'create:incomplete_precision_mechanism').processingTime(600),

		event.recipes.createFilling('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', Fluid.of('thermal:resin',50)]),

		event.recipes.createDeploying('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', 'mekanism:alloy_infused']),

		event.recipes.createDeploying('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', 'minecraft:book']),

		event.recipes.createFilling('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', Fluid.of('thermal:resin',50)]),
		event.recipes.createPressing('create:incomplete_precision_mechanism','create:incomplete_precision_mechanism')
	]).transitionalItem('create:incomplete_precision_mechanism').loops(2)

	event.recipes.createSequencedAssembly([
		Item.of('croptopia:apple_juice')
	], 'minecraft:apple', [
		event.recipes.createCutting('create:incomplete_precision_mechanism', 'create:incomplete_precision_mechanism').processingTime(200),

		event.recipes.createFilling('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', Fluid.of('minecraft:water',50)]),

		event.recipes.createPressing('create:incomplete_precision_mechanism','create:incomplete_precision_mechanism'),

		event.recipes.createDeploying('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', 'croptopia:food_press']),

		event.recipes.createDeploying('create:incomplete_precision_mechanism',['create:incomplete_precision_mechanism', 'minecraft:glass_bottle'])
	]).transitionalItem('create:incomplete_precision_mechanism').loops(1)
	
	// IF Modify
	event.custom({"input": [
		{
		  "item": "mekanism:advanced_bin"
		},
		{
		  "tag": "mekanism:personal_storage"
		},
		{
		  "item": "mekanism:advanced_bin"
		},
		{
		  "item": "thermal:lapis_gear"
		},
		{
		  "item": "thermal:rose_gold_gear"
		},
		{
		  "tag": "forge:storage_blocks/steel"
		},
		{
		  "tag": "forge:plastic"
		},
		{
		  "tag": "industrialforegoing:machine_frame/simple"
		}
		],
		"inputFluid": "{FluidName:\"industrialforegoing:pink_slime\",Amount:2000}",
		"processingTime": 1200,
		"output": {
			"item": "industrialforegoing:simple_black_hole_unit",
			"count": 1
		},
		"type": "industrialforegoing:dissolution_chamber"
		})

	// Farmer Delight Modify
	event.custom({
		"type": "farmersdelight:cooking",
		"recipe_book_tab": "meals",
		"ingredients": [{
			"item": "minecraft:apple"
		},
		{
			"item": "minecraft:sugar"
		},
		{
			"item": "minecraft:egg"
		},
		{
			"tag": "forge:flour"
		},
		{
			"item": "croptopia:dough"
		}],
		"result": {
			"item": "croptopia:apple_pie"
		},
		"experience": 0.6,
		"cookingtime": 300
	})

	event.custom({
		"type": "farmersdelight:cooking",
		"recipe_book_tab": "drinks",
		"container": {
			"item": "minecraft:bowl"
		},
		"ingredients": [{
			"item": "croptopia:oat"
		},
		{
			"item": "minecraft:sugar"
		},
		{
			"tag": "forge:milks"
		}],
		"result": {
			"item": "croptopia:oatmeal"
		},
		"experience": 0.6,
		"cookingtime": 300
	})
})

onEvent('item.tags', event => {
	// Get the #forge:cobblestone tag collection and add Diamond Ore to it
	// event.get('forge:cobblestone').add('minecraft:diamond_ore')

	// Get the #forge:cobblestone tag collection and remove Mossy Cobblestone from it
	// event.get('forge:cobblestone').remove('minecraft:mossy_cobblestone')
})
