onEvent('item.registry', event => {
    event.create('ba_ingot').fireResistant(true)
})

onEvent('recipes', event => {
    event.remove({output: '#minecraft:slabs', type: 'create:automatic_shaped'})
	event.remove({output: 'thermal:sawdust_block', type: 'create:automatic_shaped'})
    event.remove({output: '#minecraft:walls', type: 'create:automatic_shaped'})
    event.remove({input: 'minecraft:vine', type: 'minecraft:crafting_shaped'})
	event.remove({input: 'minecraft:vine', type: 'mekanism:crusher'})
    event.remove({output: '#minecraft:walls', type: 'minecraft:crafting_shaped'})
    event.remove({output: '#minecraft:walls', type: 'minecraft:crafting_shaped'})
})

onEvent('item.tags', event => {
    event.add('forge:dusts/salt', 'croptopia:salt')
    event.add('forge:dusts', 'croptopia:salt')
    event.add('#forge:flour/wheat', 'croptopia:flour')
})
